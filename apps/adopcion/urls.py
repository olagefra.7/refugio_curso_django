from django.urls import path
from apps.adopcion.views import index_adopcion, SolicitudList,SolicitudCreate, SolicitudUpdate, SolicitudDelete
app_name = "solicitud"
urlpatterns = [
    path('',index_adopcion, name='index_adopcion'),
    path('listar',SolicitudList.as_view(), name='solicitud_listar'),
    path('crear',SolicitudCreate.as_view(), name='solicitud_crear'),
    path('editar/<pk>',SolicitudUpdate.as_view(), name='solicitud_editar'),
    path('eliminar/<pk>',SolicitudDelete.as_view(), name='solicitud_eliminar'),
]